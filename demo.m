clear all
close all
clc



%% Generation of the data 
L=200; % number of light output bins
K=9; % Number of sources in the spectral library
A=rand(L,9);
A=A./(ones(L,1)*sum(A,1)); % Normalization of the sources by their integral
x0=zeros(K,1);
x0(2)=0.1;
x0(5)=0.9;
scaling_factor=1000; % arbitrary scaling factor modeling the integration time or strength of the sources
y=poissrnd(scaling_factor*A*x0);

%% Input parameters
Niter=100; % Maximum number of iterations 

% This algorithm uses a Bernoulli truncated Gaussian prior for the mixture
% coefficients. In the original paper we used the same sparsity level and
% variance for all the coefficients. However, these can be set to different
% values 
pi0=1/K*ones(K,1); % Each source as a prior probability of 1/K to be present
sig20=0.1*ones(K,1); % (hidden) variances of the truncated Gaussian distributions 



[x_out SIG_out pi_out iter]=sparse_unmixing_Poisson_EP_BTG(y,sum(y)*A,pi0,sig20,Niter);


figure
subplot(221)
plot(x0,'r')
hold on
plot(x_out,'b')
xlabel('Source #')
legend('Ground truth', 'Estimated')
title('Mixing coefficients')
subplot(222)
imagesc(SIG_out)
axis image
colorbar
title('Approx. post. covariance matrix')
xlabel('Source #')
ylabel('Source #')
subplot(223)
plot(x0>0,'r')
hold on
plot(pi_out,'b')
xlabel('Source #')
legend('Ground truth', 'Estimated')
title('Post. prob. of source presence')
subplot(224)
plot(y,'k')
hold on
plot(sum(y)*A*x_out,'r')
xlabel('Light output bin #')
legend('Observations','Approximation')


