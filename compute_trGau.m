function [mu_out sig2_out]=compute_trGau(mu,sig2)
% Computes the mean and variance of a truncated Gaussian distribution
% defined on R^+ (x>0)

% mu: mean of original Gaussian distribution 
% sig2: variance of original Gaussian distribution 

% mu_out: mean of the truncated Gaussian distribution 
% sig2_out: variance of the truncated Gaussian distribution

N=length(mu);

alp0=-mu./sqrt(sig2);
alp=alp0;

mu_out=zeros(N,1);
sig2_out=mu_out;
ind0=find(alp<7);
ind1=find(alp>=7);

if ~isempty(ind0)
    Z0=1-normcdf(alp(ind0));
    mu_out(ind0)=mu(ind0) +sqrt(sig2(ind0)).*normpdf(alp(ind0))./Z0;
    sig2_out(ind0)=sig2(ind0).*(1+alp(ind0).*normpdf(alp(ind0))./Z0 -(normpdf(alp(ind0))./Z0).^2);
end
if ~isempty(ind1)
    alp=alp0(ind1);
    b=alp.^9./(alp.^8-alp.^6+3*alp.^4-15*alp.^2+105); % approx normpdf(alp)/Z0
    mu_out(ind1)=max(mu(ind1) +sqrt(sig2(ind1)).*b,1e-7);
    sig2_out(ind1)=sig2(ind1).*(1+alp.*b -b.^2);
end

if sum(sig2_out<0)>0
    sig2_out=sig2;
end


