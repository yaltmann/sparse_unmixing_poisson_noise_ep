## Sparse_unmixing_Poisson_noise_EP

**Paper title:**

"Expectation-propagation for weak radionuclide identification at radiation portal monitors"

**Authors:**

Y. Altmann, A. Di Fulvio, M. G. Paff, S. D. Clarke, M. E. Davies, S. McLaughlin, A. O. Hero, S. A. Pozzi

**Published in:**

Scientific Reports

**Link to pdf:**

(to be updated)

## How to run this demo

Open code in MATLAB
Run the script demo.m
