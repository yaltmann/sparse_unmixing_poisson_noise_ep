function [logI I]=approx_Ex2_PG(y,b,m,s2)


h1 = @(x) 2*log(x)+ y*log(x+b) -x-b- (x-m).^2/(2*s2);
h1p = @(x) 2./x + y./(x+b) -1 -(x-m)/s2;
h1pp = @(x) -2./(x.^2) -y./((x+b).^2)-1/s2;

%%init x0
x0=1;
x1=x0+1;
err=1;
while err>10e-5
%     for t=1:10
x1=max(x0-h1p(x0)./h1pp(x0),1e-7);
err=norm(x1-x0)/norm(min(x0,x1));
% h1(x1)
x0=x1;
end

hx0=h1(x0);

hpp0=h1pp(x0);

sig0=1/sqrt(-hpp0);
L=max(-1,(1e-9 - x0)/(sig0*sqrt(2)));
while h1(x0+L*sig0*sqrt(2))-hx0 >log(1e-15) && L~= ((1e-9 - x0)/(sig0*sqrt(2)))
    L=max(L*2,(1e-9 - x0)/(sig0*sqrt(2)));
end
U=1;
while h1(x0+U*sig0*sqrt(2))-hx0 >log(1e-15)
    U=U*2;
end
N=4;
uu=linspace(L,U,N);
du=(U-L)/(N-1);
H=h1(x0+uu*sig0*sqrt(2))-hx0;
I0=du/2*(sum(2*exp(H))-exp(H(1))-exp(H(end)));
N=N*2;
uu=linspace(L,U,N);
du=(U-L)/(N-1);
H=h1(x0+uu*sig0*sqrt(2))-hx0;
I1=du/2*(sum(2*exp(H))-exp(H(1))-exp(H(end)));

while (abs(I1-I0)/min(I0,I1))>1e-5
    I0=I1;
    N=N*2;
    uu=linspace(L,U,N);
du=(U-L)/(N-1);
H=h1(x0+uu*sig0*sqrt(2))-hx0;
I1=du/2*(sum(2*exp(H))-exp(H(1))-exp(H(end)));
end

logI=hx0+log(sig0*sqrt(2))+log(I1);
I=exp(logI);
