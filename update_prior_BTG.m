function [m0 S0 p]=update_prior_BTG(m,sig2,s20,p0)

P=1./(1+exp(-p0));


gam2=sig2.*s20./(sig2+s20);
mu=m.*gam2./sig2;
del=sqrt(sig2+s20);
[alp1,s1]=compute_trGau(mu,gam2);
logPI=log(2*P./del)-log(sqrt(2*pi))-1/2*(m./del).^2+log(normcdf(m.*sqrt(gam2)./sig2));
ind=find(m.*sqrt(gam2)./sig2<-5);
if ~isempty(ind)
    logPI(ind)=log(2*P./del(ind))-log(sqrt(2*pi))-1/2*(m(ind)./sig2(ind)).^2-log(-m(ind).*sqrt(gam2(ind))./sig2(ind));
end

logK=log((1-P)./sqrt(sig2))-log(sqrt(2*pi))-1/2*(m./sqrt(sig2)).^2;
PIovZ=1./(1+exp(logK-logPI));

M_tilted=PIovZ.*alp1;
s2_tilted=PIovZ.*(alp1.^2+s1)-M_tilted.^2;
ind=find(PIovZ>(1-1e-4));
if ~isempty(ind)
    PIovZ(ind)=1-1e-4;
    M_tilted(ind)=PIovZ(ind).*alp1(ind);
    s2_tilted(ind)=0.999*s1(ind);
end
ind=find(PIovZ<1e-4);
if ~isempty(ind)
    PIovZ(ind)=1e-4;
    M_tilted(ind)=PIovZ(ind).*alp1(ind);
    s2_tilted(ind)=PIovZ(ind).*(alp1(ind).^2+s1(ind))-M_tilted(ind).^2;
end

s2_tilted(s2_tilted>0.999*sig2)=0.999*sig2(s2_tilted>0.999*sig2);
S0=1./(1./s2_tilted-1./sig2);

m0=S0.*(M_tilted./s2_tilted-m./sig2);

S0(S0<0)=s20;

p=-p0-log(1./PIovZ-1);
p(isnan(p))=10;
p(isinf(p))=10;





