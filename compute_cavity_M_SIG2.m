function [m1 SIG1]=compute_cavity_M_SIG2(m,SIG,mu,s,f)

mp=(mu/s)*f;
SIGf=SIG*f;
SIG1=SIG-SIGf*SIGf'/(f'*SIGf-s);
m1=SIG1*(SIG\m -mp);