function [mu,s2]=approx_Poiss(y,b,m,sigma2)

N=length(y);

ind0=find(y==0);
ind1=find(y>0);
mu=zeros(N,1);
s2=mu;

if ~isempty(ind0)
    [mp,s2p]=compute_trGau(m(ind0)-sigma2(ind0),sigma2(ind0));
    logPI=log(normcdf(-m(ind0)./sqrt(sigma2(ind0))));
    log1mPI=log(1-normcdf(-(m(ind0)-sigma2(ind0))./sqrt(sigma2(ind0))))+0.5*sigma2(ind0)-m(ind0);
    [mm,s2m]=compute_trGau(-m(ind0),sigma2(ind0));
    mm=-mm;
    logM=max([logPI log1mPI],[],2);
    log1mPI=log1mPI-logM;
    logPI=logPI-logM;
    Pim=exp(logPI-log(exp(logPI)+exp(log1mPI)));
    mu(ind0)=Pim.*(mm)+(1-Pim).*(mp);
    s2(ind0)=-mu(ind0).^2+ Pim.*(mm.^2+s2m)+(1-Pim).*(mp.^2+s2p);
    s2(ind0)=min(max(1e-7,s2(ind0)),1e6);
end

if ~isempty(ind1)
    y1=y(ind1);
    b1=b(ind1);
    m1=m(ind1);
    N1=length(ind1);
    m3=zeros(N1,1);
    s3=m3;
    for n=1:N1
        [logI0]=approx_z_PG(y1(n),b1(n),m1(n),sigma2(n));
        [logI1]=approx_Ex_PG(y1(n),b1(n),m1(n),sigma2(n));
        [logI2]=approx_Ex2_PG(y1(n),b1(n),m1(n),sigma2(n));
        Ex=exp(logI1-logI0);
        Ex2=exp(logI2-logI0);
        vx=Ex2-Ex^2;
        m3(n)=Ex;
        s3(n)=min(max(1e-7,vx),1e6);
    end
    mu(ind1)=m3;
    s2(ind1)=s3;
end



