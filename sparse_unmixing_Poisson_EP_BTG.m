function [m SIG p_out m_compt]=sparse_unmixing_Poisson_EP_BTG(y,F,pi0,sig20,Niter)

eta=0.7; % Damping parameter
[L,K]=size(F);

y=max(0,floor(y)); %(to make sure y is discrete and positive)

p=zeros(K,1);
p0=log(pi0./(1-pi0));
p_out=1./(1+exp(-(p+p0)));
H=eye(K);

%% Initialization (first pass);
s=zeros(L,1);
mk=zeros(K,1);
sk=zeros(K,1);
for k=1:K
    [mk(k),sk(k)]=compute_trGau(0,sig20(k));
end

Mu=y+1;
s=y+1;
logZ=1;
SIG=inv(diag(1./sk)+F'*diag(1./s)*F);
m=(((F'*(Mu./s))'+(mk./sk)')*SIG)';

%% Iterations
m_compt=1;
thres=1;
while ((m_compt<=Niter && thres>1e-6) || m_compt<40)
    m0=m;
    SIG0=SIG;

    % 1) update likelihood subgraph 
    for i=randperm(L)
        % Compute cavity
        [m1 SIG1]=compute_cavity_M_SIG2(m,SIG,Mu(i),s(i),F(i,:)');

        % compute mean and variance of ui=fi'*x
        m_u=F(i,:)*m1;
        sig2_u=F(i,:)*(SIG1*F(i,:)');

        % Minimize Divergence  
        [mm,ss]=approx_Poiss(y(i),0,m_u,sig2_u);
        if ss>0.999*sig2_u
            ss=0.999*sig2_u;
        end
        
        % Update factor with damping
        s1(i)=1./(1./ss-1./sig2_u);
        Mu1(i)=s1(i)*(mm/ss-m_u/sig2_u);
        s00=s(i);
        s(i)=1./(eta./s1(i)+(1-eta)./s00);
        Mu(i)=s(i).*(eta*Mu1(i)./s1(i)+(1-eta)*Mu(i)./s00);
        
        % Update global approximation
        [m SIG]=update_approx(s(i),Mu(i),F(i,:)',m1,SIG1,logZ);

    end
    
    % 2) update prior subgraph 
    for k=randperm(K)
        
        % Compute Cavity
       [m2 SIG2]=compute_cavity_M_SIG2(m,SIG,mk(k),sk(k),H(k,:)');
       
        % Compute mean and variance current factor
        m_u=m2(k);
        sig2_u=SIG2(k,k);

        % Minimize Divergence
            [Mu2(k) s2(k) p(k)]=update_prior_BTG(m_u,sig2_u,sig20(k),p0(k));
        
        % Update factor with damping
        s00=sk(k);
        sk(k)=1./(eta./s2(k)+(1-eta)./s00);
        mk(k)=sk(k).*(eta*Mu2(k)./s2(k)+(1-eta)*mk(k)./s00);
        
        % Update global approximation
        [m SIG]=update_approx(sk(k),mk(k),H(k,:)',m2,SIG2,logZ);
         
    end
    
    p_out=1./(1+exp(-(p+p0)));
    
    if m_compt>5
        thres=norm(m-m0)+norm(SIG-SIG0);
    end
    m_compt=m_compt+1;
end