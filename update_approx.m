function [m_out SIG_out logZt]=update_approx(s2,mu,h,m,SIG,logZ)
% m_out=M;
% SIG_out=SIG;
logZt=1;
% SIG_out=real(inv( (h*h')/s2 + inv(SIG)));
SIG_out=SIG-(SIG*h)*(h'*SIG)/(s2+h'*SIG*h);
m_out=real(((mu*h'/s2+(m'/SIG))*SIG_out)');
% logZt=logZ+1/2*(log(2*pi*s2)+log(1+(h'*(SIG*h))/s2) + (m'/SIG)*m + mu^2/s2 -(m_out'/SIG_out)*m_out);


